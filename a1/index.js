//console.log("Hello World!")

//3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

const getCube = 2 ** 3;

// Template Literals
// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
// 5. Create a variable address with a value of an array containing details of an address.
// 6. Destructure the array and print out a message with the full address using Template Literals.

let messageA = `The cube of 2 is ${getCube}`
console.log(messageA);

const myAddress = ["258","Washington Ave NW","California",90011];

const [num,ave,state,zip] = myAddress;

console.log(`I live at ${num} ${ave} ${state} ${zip}`)


// Object Destructuring
// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.

const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	height: "20 ft 3 in"
};

const {name, type, weight, height} = animal;

console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${height}.`)

// Arrow Functions
//9. Create an array of numbers.

const myNumbers = ["1","2","3","4","5"];


// A
//10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

myNumbers.forEach((myNumbers)=>{
	console.log(myNumbers)
})

// B
// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

const reduceNumber = (a,b,c,d,e) => a+b+c+d+e;

let total = reduceNumber(1,2,3,4,5);
console.log(total);


// Javascript Objects
// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// 13. Create/instantiate a new object from the class Dog and console log the object.

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();

myDog.name = "Burrito";
myDog.age = 1;
myDog.breed = "Bichon Frise";

console.log(myDog);